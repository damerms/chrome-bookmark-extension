
function click(e) {
    chrome.tabs.executeScript(null, //5.execute the script
     {code:"document.style.backgroundImage='url(" + images [e.target.id] + "'"}); //change the color to the color clicked 
     window.close(); //close the window
}

document.addEventListener('DOMContentLoaded', function(){//1. when dom is completely loaded
    var divs = document.querySelectorAll('div'); //2. find all divs
    for (var i = 0; i < divs.length; i++) { // 3. loop through all the divs
        divs.addEventListener('click', click); //4. wait for the click   
    }
});

var images = {
    Joe: 'https://cdn.dribbble.com/users/739995/screenshots/1998556/emoji_dribbble-02.png',
    Kate: 'http://wrapacrap.com/wp-content/uploads/2016/10/200_s-1.gif',
    David: 'http://www.ktk985.com/sites/g/files/giy1056/f/styles/delta__775x515/public/General/775X515-POO-EMOJI.png?itok=ZQs4s_p-'
};
